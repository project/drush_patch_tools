<?php

/**
 * Implementation of hook_drush_command().
 */
function drush_patch_tools_drush_command() {
  $items['patch'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => 'Downloads and applies a patch.',
    'arguments' => array(
      'patch' => 'Filename of the patch, URL of the patch, issue URL of the patch, issue number of the patch.',
    ),
    'examples' => array(
      'drush patch 687686' => 'Applies the latest patch from issue #687686 on drupal.org.',
      'drush patch http://drupal.org/node/687686' => 'Applies the latest patch from issue #687686 on drupal.org.',
      'drush patch http://drupal.org/files/issues/remove_default_content_687686.patch' => 'Applies the patch at the given URL.',
      'drush patch remove_default_content_687686.patch' => 'Applies the patch.',
    ),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function drush_patch_tools_drush_help($section) {
  switch ($section) {
    case 'drush:patch':
      return dt('Downloads and applies patches.');
  }
}

/**
 * Drush callback; download the patch and apply it.
 */
function drush_drush_patch_tools_patch($patch) {
  $file = drush_patch_tools_get_patch($patch);
  drush_shell_exec('patch -p0 < ' . $file);
}

/**
 * Helper to get file contents.
 */
function _drush_patch_tools_get_remote_file($url) {
  if (function_exists('curl_init')) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    $file_contents = curl_exec($ch);
    curl_close($ch);
    return $file_contents;
  }
  elseif (ini_get('allow_url_fopen')) {
    return file_get_contents($url);
  }
  elseif (drush_shell_exec('wget -qO- %s', $url) || drush_shell_exec('curl -L --silent %s', $url)) {
    return implode("\n", drush_shell_exec_output());
  }
  else {
    return drush_set_error('drush_patch_tools_download_error', 'Unable to download remote file. No download services supported.');
  }
}

function drush_patch_tools_get_patch($patch) {
  $nid = FALSE;
  $url = FALSE;
  $file = FALSE;
  if (is_numeric($patch)) {
    $nid = $patch;
  }
  elseif (preg_match('/http:\/\/drupal\.org\/node\/(\d+).*/', $patch, $matches)) {
    $nid = $matches[1];
  }
  elseif (file_exists($patch)) {
    $file = $patch;
  }
  else {
    $url = $patch;
  }
  if ($nid) {
    $html = _drush_patch_tools_get_remote_file('http://drupal.org/node/' . $nid);
    // DOM can load HTML soup. But, HTML soup can throw warnings, suppress
    // them.
    @$htmlDom = DOMDocument::loadHTML($html);
    if ($htmlDom) {
      // It's much easier to work with simplexml than DOM, luckily enough
      // we can just simply import our DOM tree.
      $elements = simplexml_import_dom($htmlDom);
      foreach ($elements->xpath("//div") as $div) {
        if (strpos((string)$div['id'], 'pift-results') === 0) {
          // We have a match
          $row = FALSE;
          foreach ($div->table->tbody->tr as $tr) {
            $row = $tr;
          }
          if (strpos((string)$tr['class'], 'pass') !== FALSE) {
            $url = (string)$row->td[0]->a['href'];
          }
        }
      }
    }
  }
  if ($url) {
    list($file) = array_reverse(explode('/', $url));
    (drush_shell_exec('wget %s -O %s', $url, $file) || drush_shell_exec('curl -L -o %s %s', $file, $url));
  }
  return $file;
}
